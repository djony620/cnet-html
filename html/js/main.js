(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 70)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('#search').click(function() {
    $('#search_div').toggleClass('hide');
  });
  $('#search_close').click(function() {
    $('#search_div').toggleClass('hide');
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('#show_icon').click(function() {
    $('#social_icons').find('li.s_hide').toggleClass('hide', 1000);
    $(this).find('i').toggleClass('fa-times');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 100
  });

  // fixed nav script
  var s = $("#nav");
  var pos = s.position();	
  $(window).scroll(function () {
    var windowpos = $(window).scrollTop();
    if (windowpos >= pos.top) {
      s.css({"position": "fixed","top" : "0","background-color" : "#fff", "border-bottom":"1px solid #ddd"});
      $('.exo-menu > li > a').css('color','#000');
    } else {
      s.css({ "position": "absolute", "top": "", "background-color": "", "border-bottom": "0" });
      $('.exo-menu > li > a').css('color', '#fff');
    }
  });

  // // Collapse Navbar
  // var navbarCollapse = function() {
  //   if ($("#mainNav").offset().top > 100) {
  //     $("#mainNav").addClass("navbar-shrink");
  //   } else {
  //     $("#mainNav").removeClass("navbar-shrink");
  //   }
  // };

  $('#return-to-top').click(function () {      // When arrow is clicked
    $('body,html').animate({
      scrollTop: 0                       // Scroll to top of body
    }, 500);
  });

  $(document).ready(function () {
    $("#back-top").hide();
    $(function () {
      $(window).scroll(function () {
        if ($(this).scrollTop() > 250) {
          $('#back-top').fadeIn();
        } else {
          $('#back-top').fadeOut();
        }
      });
      $('#back-top').click(function () {
        $('body,html').animate({
          scrollTop: 0
        }, 800);
        return false;
      });
    });
  });


  $(function () {
    $('.toggle-menu').click(function () {
      $('.exo-menu').toggleClass('display');
    });
  });

  $(document.body).on('click','#search',function() {
    $('#search_div').show();
  });
  // Collapse now if page is not at top
  // navbarCollapse();
  // Collapse the navbar when page is scrolled
  // $(window).scroll(navbarCollapse);

  /*================================
  countdown
  ==================================*/
  $('[data-countdown]').each(function () {
      var $this = $(this),
          finalDate = $(this).data('countdown');
      $this.countdown(finalDate, function (event) {
          $this.html(event.strftime('<span class="cdown days"><span class="time-count">%-D</span> <p>Days</p></span> <span class="cdown hour"><span class="time-count">%-H</span> <p>Hour</p></span> <span class="cdown minutes"><span class="time-count">%M</span> <p>Min</p></span> <span class="cdown second"> <span><span class="time-count">%S</span> <p>Sec</p></span>'));
      });
  });

  /*================================
    Owl Carousel
    ==================================*/
    // main slider active
    if ($('.prod-slider').length) {
      var owl = $(".prod-slider");
      owl.owlCarousel({
          margin: 15,
          loop: true,
          items: 5,
          responsive : {
              0 : {
                items: 1,
              },
              480 : {
                items: 1,
              },
              768 : {
                items: 2,
              },
              1024 : {
                items: 5,
              },
              1360 : {
                items: 5,
              }
          },
          dots: false,
          autoplay: true,
          autoplayTimeout: 4000,
          nav: true,
          smartSpeed: 800,
          navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
      });
    }
    // deals slider active
    if ($('.deals-slider').length) {
      var owl = $(".deals-slider");
      owl.owlCarousel({
          margin: 15,
          loop: true,
          items: 5,
          responsive : {
              0 : {
                items: 1,
              },
              480 : {
                items: 1,
              },
              768 : {
                items: 2,
              },
              1024 : {
                items: 4,
              },
              1360 : {
                items: 4,
              }
          },
          dots: false,
          autoplay: true,
          autoplayTimeout: 4000,
          nav: true,
          smartSpeed: 800,
          navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
      });
    }

  /*================================
   slicknav Mobile Menu
  ==================================*/
  $('ul#nav_menu').slicknav({
      prependTo: ".mobile_menu"
  });

  /*================================
   Show Hide Comment box
  ==================================*/
  $('.cmnt-toggle-btn').on('click', function () {
    $('.comment-box-area').addClass('show_hide');
    $('.overlay').addClass('show_hide');
  })

  $('.drawer-close').on('click', function () {
    $('.comment-box-area').removeClass('show_hide');
    $('.overlay').removeClass('show_hide');
  })





})(jQuery); // End of use strict
